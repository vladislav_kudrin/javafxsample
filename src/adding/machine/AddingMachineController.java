package adding.machine;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import adding.machine.animation.Shake;

/**
 * Controls addingMachine.fxml.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/17/2020
 */
public class AddingMachineController {
    @FXML
    private Label firstOperandLabel;

    @FXML
    private TextField firstOperand;

    @FXML
    private Label secondOperandLabel;

    @FXML
    private TextField secondOperand;

    @FXML
    private Button addButton;

    @FXML
    private Button clearButton;

    @FXML
    private Label resultLabel;

    @FXML
    private TextField result;

    @FXML
    void add() {
        if(firstOperand.getText().isEmpty() || secondOperand.getText().isEmpty()) {
            Shake operandShake = (firstOperand.getText().isEmpty()) ? new Shake(firstOperand) : new Shake(secondOperand);
            operandShake.playAnimation();
            return;
        }

        int first = Integer.parseInt(firstOperand.getText());
        int second = Integer.parseInt(secondOperand.getText());
        result.setText(String.valueOf(first + second));
    }

    @FXML
    void clear() {
        firstOperand.clear();
        secondOperand.clear();
        result.clear();
    }
}