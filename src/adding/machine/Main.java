package adding.machine;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Runs an application's window.
 *
 * @author Vladislav
 * @version 1.0
 * @since 04/17/2020
 */
public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("addingMachine.fxml"));
        primaryStage.getIcons().add(new Image("adding\\machine\\assets\\icon.png"));
        primaryStage.setTitle("Adding Machine");
        primaryStage.setScene(new Scene(root, 550, 550));
        primaryStage.setResizable(false);
        primaryStage.show();
    }


    public static void main(String[] args) {
        launch(args);
    }
}